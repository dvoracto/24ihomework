package cz.tomasdvoracek.a24ihomework.injection

import cz.tomasdvoracek.a24ihomework.Config
import cz.tomasdvoracek.a24ihomework.domain.helpers.RetrofitHelper
import cz.tomasdvoracek.a24ihomework.domain.repository.MovieRepository
import cz.tomasdvoracek.a24ihomework.domain.useCases.GetMovieDetailUseCase
import cz.tomasdvoracek.a24ihomework.domain.useCases.GetMovieUpdatesUseCase
import cz.tomasdvoracek.a24ihomework.domain.webservice.MovieWebService
import cz.tomasdvoracek.a24ihomework.ui.movieDetailActivity.MovieDetailViewModel
import cz.tomasdvoracek.a24ihomework.ui.movieListActivity.MovieListViewModel
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object AppModule {
    val module = module {

        single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC } }
        single { RetrofitHelper.createOkHttpClient() }

        single { RetrofitHelper.createWebService<MovieWebService>(get(), Config.SERVER_URL) }

        single { MovieRepository(get()) }
        single { GetMovieUpdatesUseCase(get()) }
        single { GetMovieDetailUseCase(get()) }
        viewModel { MovieListViewModel(get(), get()) }
        viewModel { MovieDetailViewModel() }
    }
}