package cz.tomasdvoracek.a24ihomework.domain.useCases

import cz.tomasdvoracek.a24ihomework.domain.repository.MovieRepository
import cz.tomasdvoracek.a24ihomework.model.Movie
import io.reactivex.Maybe

class GetMovieDetailUseCase(private val movieRepository: MovieRepository) {

    fun getMovieDetail(movieId: Long): Maybe<Movie> {
        return movieRepository.getMovieDetail(movieId).flatMap {
            Maybe.fromCallable {
                return@fromCallable Movie(
                    it.id,
                    it.adult,
                    it.title,
                    it.cover,
                    it.originalLanguage,
                    it.genres?.map { it.name },
                    it.overview,
                    it.releaseDate
                )
            }
        }
    }
}