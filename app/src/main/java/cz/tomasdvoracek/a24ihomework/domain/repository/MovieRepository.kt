package cz.tomasdvoracek.a24ihomework.domain.repository

import cz.tomasdvoracek.a24ihomework.domain.webservice.MovieWebService
import cz.tomasdvoracek.a24ihomework.model.response.MovieDetailResponse
import cz.tomasdvoracek.a24ihomework.model.response.MovieUpdateResponse
import io.reactivex.Maybe
import io.reactivex.Single

class MovieRepository(private val movieWebService: MovieWebService) {

    fun getMovieIds(): Single<MovieUpdateResponse> =
        movieWebService.getUpdatedMovies()

    fun getMovieDetail(movieId: Long): Maybe<MovieDetailResponse> =
        movieWebService.getMovieDetail(movieId)

}