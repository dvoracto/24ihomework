package cz.tomasdvoracek.a24ihomework.domain.webservice

import cz.tomasdvoracek.a24ihomework.Config
import cz.tomasdvoracek.a24ihomework.model.response.MovieDetailResponse
import cz.tomasdvoracek.a24ihomework.model.response.MovieUpdateResponse
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieWebService {

    @GET("movie/changes?api_key=${Config.API_KEY}&page=1")
    fun getUpdatedMovies(): Single<MovieUpdateResponse>

    @GET("movie/{id}?api_key=${Config.API_KEY}")
    fun getMovieDetail(@Path("id") id: Long): Maybe<MovieDetailResponse>

}