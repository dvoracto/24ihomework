package cz.tomasdvoracek.a24ihomework.domain.useCases

import cz.tomasdvoracek.a24ihomework.domain.repository.MovieRepository
import io.reactivex.Single

class GetMovieUpdatesUseCase(private val movieRepository: MovieRepository) {

    fun getMovieIds(): Single<List<Long>> {
        return movieRepository.getMovieIds().flatMap {
            Single.fromCallable {
                val movieIds = mutableListOf<Long>()
                it.results.filter { !it.adult }.map { m -> movieIds.add(m.id) }
                return@fromCallable movieIds
            }
        }
    }
}