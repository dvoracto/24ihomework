package cz.tomasdvoracek.a24ihomework.utils.mvvm

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import cz.tomasdvoracek.a24ihomework.BR
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class MvvmActivity<T : ViewModelBase, TBinding : ViewDataBinding>(viewModelClass: KClass<T>, @LayoutRes val layout: Int) : AppCompatActivity() {

    protected lateinit var binding: TBinding

    protected val viewModel: T by lazy {
        getViewModel(viewModelClass)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout)
        binding.setVariable(BR.viewmodel, viewModel)
        binding.lifecycleOwner = this
    }
}