package cz.tomasdvoracek.a24ihomework.utils.mvvm

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class ViewModelBase : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}