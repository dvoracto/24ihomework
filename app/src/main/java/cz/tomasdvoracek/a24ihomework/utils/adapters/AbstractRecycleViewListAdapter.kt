package cz.tomasdvoracek.a24ihomework.utils.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractRecycleViewListAdapter<T, TItemBinding : ViewDataBinding>(@LayoutRes val itemLayoutId: Int) :
    RecyclerView.Adapter<AbstractRecycleViewViewHolder>() {

    private var data: MutableList<T> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractRecycleViewViewHolder {
        return AbstractRecycleViewViewHolder(LayoutInflater.from(parent.context).inflate(itemLayoutId, parent, false))
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: AbstractRecycleViewViewHolder, position: Int) {
        val safeHolder = holder as? AbstractRecycleViewViewHolder? ?: return
        val binding = DataBindingUtil.bind<TItemBinding>(safeHolder.itemView) ?: return

        setData(binding, data[position], position)
    }

    abstract fun setData(binding: TItemBinding, item: T, position: Int)

    open fun putDataToRecycleView(data: MutableList<T>) {
        this.data = data
        notifyDataSetChanged()
    }

    open fun addItemToList(data: T) {
        this.data.add(data)
        notifyItemInserted(this.data.size)
    }

    fun clear() {
        this.data = mutableListOf()
        notifyDataSetChanged()
    }
}