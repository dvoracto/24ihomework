package cz.tomasdvoracek.a24ihomework.utils.mvvm

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("app:visible")
fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}