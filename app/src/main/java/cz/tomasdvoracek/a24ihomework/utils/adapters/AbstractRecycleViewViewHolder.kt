package cz.tomasdvoracek.a24ihomework.utils.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class AbstractRecycleViewViewHolder(view: View) : RecyclerView.ViewHolder(view)