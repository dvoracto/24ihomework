package cz.tomasdvoracek.a24ihomework.ui.movieDetailActivity

import android.os.Bundle
import android.view.MenuItem
import cz.tomasdvoracek.a24ihomework.R
import cz.tomasdvoracek.a24ihomework.databinding.ActivityMovieDetailBinding
import cz.tomasdvoracek.a24ihomework.model.Movie
import cz.tomasdvoracek.a24ihomework.setImage
import cz.tomasdvoracek.a24ihomework.utils.mvvm.MvvmActivity


class MovieDetailActivity
    : MvvmActivity<MovieDetailViewModel, ActivityMovieDetailBinding>
(MovieDetailViewModel::class, R.layout.activity_movie_detail) {

    companion object {
        const val MOVIE_DATA = "movie_data"
        const val TRANSITION_NAME = "transition_name"
    }

    private lateinit var movie: Movie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        movie = intent.getSerializableExtra(MOVIE_DATA) as? Movie
                ?: throw IllegalArgumentException("Movie not sent via intent")

        binding.movieDetailImage.transitionName = intent.getStringExtra(TRANSITION_NAME)

        setupData()
    }

    private fun setupData() {
        supportPostponeEnterTransition()
        binding.movieDetailImage.setImage(
                movie.getFullCoverImageUrl(),
                { supportStartPostponedEnterTransition() },
                { supportStartPostponedEnterTransition() })

        binding.detailTitle.text = movie.title
        binding.detailLanguage.text = movie.language
        binding.detailOverview.text = movie.overview
        binding.detailGenre.text = movie.genres?.joinToString()
        binding.detailReleaseDate.text = movie.getReadableDate()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
