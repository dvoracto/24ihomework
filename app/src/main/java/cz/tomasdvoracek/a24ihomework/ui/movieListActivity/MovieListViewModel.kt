package cz.tomasdvoracek.a24ihomework.ui.movieListActivity

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import cz.tomasdvoracek.a24ihomework.default
import cz.tomasdvoracek.a24ihomework.disposeWith
import cz.tomasdvoracek.a24ihomework.domain.useCases.GetMovieDetailUseCase
import cz.tomasdvoracek.a24ihomework.domain.useCases.GetMovieUpdatesUseCase
import cz.tomasdvoracek.a24ihomework.model.Movie
import cz.tomasdvoracek.a24ihomework.utils.adapters.AbstractRecycleViewListAdapter
import cz.tomasdvoracek.a24ihomework.utils.mvvm.SingleEventLiveData
import cz.tomasdvoracek.a24ihomework.utils.mvvm.ViewModelBase
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MovieListViewModel(
        private val getMovieUpdatesUseCase: GetMovieUpdatesUseCase,
        private val getMovieDetailUseCase: GetMovieDetailUseCase
) : ViewModelBase() {

    val movieIds = SingleEventLiveData<List<Long>>()
    val movies = MutableLiveData<MutableList<Movie>>()
    private val appHasMovieIds = MutableLiveData<Boolean>()
    val downloadingError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>().default(false)

    init {
        getMovieIds()
    }

    fun getMovieIds() {
        loading.value = true
        downloadingError.value = false

        getMovieUpdatesUseCase.getMovieIds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t ->
                    loading.value = false
                    movieIds.value = t
                    appHasMovieIds.value = true
                }, {
                    loading.value = false
                    downloadingError.postValue(true)
                })
                .disposeWith(compositeDisposable)
    }

    fun downloadMovieDetails(adapter: AbstractRecycleViewListAdapter<Movie, *>, recyclerView: RecyclerView) {
        Observable.fromIterable(movieIds.value)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    val movie = getMovieDetailUseCase.getMovieDetail(it)
                            .subscribeOn(Schedulers.io())
                            .onErrorResumeNext(Maybe.empty())
                            .blockingGet()

                    if (movie != null) {
                        putMovieToList(movie)
                        recyclerView.post { adapter.addItemToList(movie) }
                    }
                }, {
                    it.printStackTrace()
                }).disposeWith(compositeDisposable)
    }

    private fun putMovieToList(movie: Movie?) {
        movie?.let {
            val list = movies.value ?: mutableListOf()
            list.add(it)
            movies.postValue(list)
        }
    }
}