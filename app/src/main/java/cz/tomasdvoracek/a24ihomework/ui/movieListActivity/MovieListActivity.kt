package cz.tomasdvoracek.a24ihomework.ui.movieListActivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import cz.tomasdvoracek.a24ihomework.R
import cz.tomasdvoracek.a24ihomework.utils.adapters.AbstractRecycleViewListAdapter
import cz.tomasdvoracek.a24ihomework.databinding.ActivityMovieListBinding
import cz.tomasdvoracek.a24ihomework.databinding.ItemMovieListBinding
import cz.tomasdvoracek.a24ihomework.model.Movie
import cz.tomasdvoracek.a24ihomework.setImage
import cz.tomasdvoracek.a24ihomework.ui.movieDetailActivity.MovieDetailActivity
import cz.tomasdvoracek.a24ihomework.utils.mvvm.MvvmActivity


class MovieListActivity :
    MvvmActivity<MovieListViewModel, ActivityMovieListBinding>
        (MovieListViewModel::class, R.layout.activity_movie_list) {

    private lateinit var adapter: AbstractRecycleViewListAdapter<Movie, *>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupRecycleView()
        setupObservers()
        setupOnClick()
    }

    private fun setupRecycleView() {
        adapter = object : AbstractRecycleViewListAdapter<Movie, ItemMovieListBinding>(
            R.layout.item_movie_list
        ) {
            override fun setData(binding: ItemMovieListBinding, item: Movie, position: Int) {
                binding.movieListTitle.text = item.title
                binding.movieListImage.setImage(item.getFullCoverImageUrl())

                binding.root.setOnClickListener {
                    val transName = "traName$position"
                    binding.movieListImage.transitionName = transName

                    Intent(this@MovieListActivity, MovieDetailActivity::class.java).apply {
                        putExtra(MovieDetailActivity.MOVIE_DATA, item)
                        putExtra(MovieDetailActivity.TRANSITION_NAME, transName)
                        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            this@MovieListActivity, binding.movieListImage as View, transName
                        )
                        startActivity(this, options.toBundle())
                    }
                }
            }
        }

        binding.movieListRecycleView.adapter = adapter
        binding.movieListRecycleView.setHasFixedSize(true)
        adapter.clear()
        adapter.putDataToRecycleView(viewModel.movies.value ?: mutableListOf())
    }

    private fun setupOnClick() {
        binding.movieListTryAgainBtn.setOnClickListener {
            viewModel.getMovieIds()
        }
    }

    private fun setupObservers() {
        viewModel.movieIds.observeOnce(this, Observer {
            viewModel.downloadMovieDetails(adapter, binding.movieListRecycleView)
        })
    }
}