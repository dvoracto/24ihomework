package cz.tomasdvoracek.a24ihomework.ui.splashScreenActivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import cz.tomasdvoracek.a24ihomework.ui.movieListActivity.MovieListActivity

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(SplashScreenViewModel::class.java)

        viewModel.logoPresented.observe(this, Observer {
            Intent(this, MovieListActivity::class.java).apply { startActivity(this);finish() }
        })
    }
}