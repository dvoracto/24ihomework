package cz.tomasdvoracek.a24ihomework.ui.splashScreenActivity

import androidx.lifecycle.MutableLiveData
import cz.tomasdvoracek.a24ihomework.disposeWith
import cz.tomasdvoracek.a24ihomework.utils.mvvm.ViewModelBase
import io.reactivex.Completable
import java.util.concurrent.TimeUnit

class SplashScreenViewModel : ViewModelBase() {

    val logoPresented = MutableLiveData<Boolean>()

    init {
        presentLogo()
    }

    private fun presentLogo() {
        Completable.timer(2000, TimeUnit.MILLISECONDS)
                .subscribe { logoPresented.postValue(true) }
                .disposeWith(compositeDisposable)
    }
}