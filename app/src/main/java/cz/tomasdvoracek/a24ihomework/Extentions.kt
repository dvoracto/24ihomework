package cz.tomasdvoracek.a24ihomework

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

fun Disposable.disposeWith(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun <T> MutableLiveData<T>.default(item: T): MutableLiveData<T> {
    this.value = item;return this
}

fun ImageView.setImage(
    path: String,
    onLoaded: (((resource: Bitmap?) -> Unit))? = null,
    onFailed: (() -> Unit)? = null
) {

    Timber.d(path)

    val listener = object : RequestListener<Bitmap> {

        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Bitmap>?,
            isFirstResource: Boolean
        ): Boolean {
            onFailed?.invoke()
            return false
        }

        override fun onResourceReady(
            resource: Bitmap?,
            model: Any?,
            target: Target<Bitmap>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            onLoaded?.invoke(resource)
            return false
        }
    }

    com.bumptech.glide.Glide.with(context).asBitmap()
        .load(path)
        .listener(listener)
        .placeholder(R.drawable.image_placeholder)
        .error(R.drawable.image_placeholder)
        .into(this)
}