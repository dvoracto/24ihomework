package cz.tomasdvoracek.a24ihomework.model.response

data class MovieUpdateResponse(
    val results: List<MovieUpdate>
)

data class MovieUpdate(
    val id: Long,
    val adult: Boolean
)