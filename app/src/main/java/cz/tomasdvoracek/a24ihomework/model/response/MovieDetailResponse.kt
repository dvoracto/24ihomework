package cz.tomasdvoracek.a24ihomework.model.response

import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
    val id: Long,
    val adult: Boolean?,
    val title: String?,
    @SerializedName("original_language") val originalLanguage: String,
    val genres: List<MovieDetailGenreResponse>?,
    val overview: String?,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("poster_path") val cover: String?
)

data class MovieDetailGenreResponse(
    val id: Long,
    val name: String
)