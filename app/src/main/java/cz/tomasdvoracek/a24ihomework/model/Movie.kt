package cz.tomasdvoracek.a24ihomework.model

import java.io.Serializable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

data class Movie(
    val id: Long,
    val adult: Boolean?,
    val title: String?,
    val cover: String?,
    val language: String?,
    val genres: List<String>?,
    val overview: String?,
    val releaseDate: String?
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Movie

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun getFullCoverImageUrl(): String {
        if (cover == null) return ""
        return "https://image.tmdb.org/t/p/w500$cover"
    }

    fun getReadableDate(): String {
        if (releaseDate.isNullOrEmpty()) return ""
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(sdf.parse(releaseDate))
    }
}